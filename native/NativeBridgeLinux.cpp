#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/extensions/XTest.h>
#include "NativeBridge.h"

JNIEXPORT void JNICALL Java_com_metaphore_shortcut_NativeBridge_pressKey(JNIEnv *env, jobject thisObj, jint keyCode) {
    Display *display;
    display = XOpenDisplay(NULL);

//    unsigned int keyCode = XKeysymToKeycode(display, keyCode);
    XTestFakeKeyEvent(display, keyCode, True, 0);
    XFlush(display);
}

JNIEXPORT void JNICALL Java_com_metaphore_shortcut_NativeBridge_releaseKey(JNIEnv *env, jobject thisObj, jint keyCode) {
    Display *display;
    display = XOpenDisplay(NULL);

//    unsigned int keyCode = XKeysymToKeycode(display, keyCode);
    XTestFakeKeyEvent(display, keyCode, False, 0);
    XFlush(display);

}

JNIEXPORT void JNICALL Java_com_metaphore_shortcut_NativeBridge_typeKey(JNIEnv *env, jobject thisObj, jint keyCode) {
    Display *display;
    display = XOpenDisplay(NULL);

//    unsigned int keyCode = XKeysymToKeycode(display, keyCode);
    XTestFakeKeyEvent(display, keyCode, True, 0);
    XTestFakeKeyEvent(display, keyCode, False, 0);
    XFlush(display);
}