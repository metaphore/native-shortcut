#define WINVER 0x0500
#include <windows.h>
#include "NativeBridge.h"

JNIEXPORT void JNICALL Java_com_metaphore_shortcut_NativeBridge_pressKey(JNIEnv *env, jobject thisObj, jint keyCode) {
    INPUT ip;

    // Set up a generic keyboard event.
    ip.type = INPUT_KEYBOARD;
    ip.ki.wScan = 0; // hardware scan code for key
    ip.ki.time = 0;
    ip.ki.dwExtraInfo = 0;

    // Press the key
    ip.ki.wVk = keyCode; // virtual-key code
    ip.ki.dwFlags = 0; // 0 for key press
    SendInput(1, &ip, sizeof(INPUT));
}

JNIEXPORT void JNICALL Java_com_metaphore_shortcut_NativeBridge_releaseKey(JNIEnv *env, jobject thisObj, jint keyCode) {
    INPUT ip;

    // Set up a generic keyboard event.
    ip.type = INPUT_KEYBOARD;
    ip.ki.wScan = 0; // hardware scan code for key
    ip.ki.time = 0;
    ip.ki.dwExtraInfo = 0;

    // Release the key
    ip.ki.wVk = keyCode; // virtual-key code
    ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
    SendInput(1, &ip, sizeof(INPUT));
}

JNIEXPORT void JNICALL Java_com_metaphore_shortcut_NativeBridge_typeKey(JNIEnv *env, jobject thisObj, jint keyCode) {
    INPUT ip;

    // Set up a generic keyboard event.
    ip.type = INPUT_KEYBOARD;
    ip.ki.wScan = 0; // hardware scan code for key
    ip.ki.time = 0;
    ip.ki.dwExtraInfo = 0;
    ip.ki.wVk = keyCode; // virtual-key code

    // Press the key
    ip.ki.dwFlags = 0; // 0 for key press
    SendInput(1, &ip, sizeof(INPUT));

    // Release the key
    ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
    SendInput(1, &ip, sizeof(INPUT));
}