package com.metaphore.shortcut;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileUtils {
    public static File copyToTempFile(String resourcePath) {
        File temp = null;
        InputStream in = null;
        FileOutputStream fos = null;
        try {
            in = ClassLoader.getSystemClassLoader().getResourceAsStream(resourcePath);
            byte[] buffer = new byte[1024];
            int read;
            temp = File.createTempFile(resourcePath, "");
            fos = new FileOutputStream(temp);

            while((read = in.read(buffer)) != -1) {
                fos.write(buffer, 0, read);
            }
            fos.close();
            in.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (in != null) try { in.close(); } catch (IOException ignore) { }
            if (fos != null) try { fos.close(); } catch (IOException ignore) { }
        }

        return temp;
    }
}
