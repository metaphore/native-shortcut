package com.metaphore.shortcut;

import java.io.File;

import static org.apache.commons.lang3.SystemUtils.*;

public class NativeBridge {

    public static void loadNatives() {
        String libFileName = null;
        if (IS_OS_WINDOWS) {
            switch (OS_ARCH) {
                case "amd64":
                    libFileName = "NativeBridge-win64.dll";
                    break;
                case "x86":
                    libFileName = "NativeBridge-win32.dll";
                    break;
            }
        } else if (IS_OS_LINUX) {
            switch (OS_ARCH) {
                case "amd64":
                    libFileName = "NativeBridge-linux64.so";
                    break;
                case "i386":
                    libFileName = "NativeBridge-linux32.so";
                    break;
            }
        }
        if (libFileName == null) {
            throw new IllegalStateException("Can't initialize native lib for " + OS_NAME + OS_ARCH);
        }

        File lib = FileUtils.copyToTempFile(libFileName);
        lib.deleteOnExit();
        System.load(lib.getAbsolutePath());
    }

    public native void pressKey(int keyCode);
    public native void releaseKey(int keyCode);
    public native void typeKey(int keyCode);
}
